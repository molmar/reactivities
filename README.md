
# Reactivities

Ez a repository egy [Udemy kurzus](https://www.udemy.com/course/complete-guide-to-building-an-app-with-net-core-and-react/) keretében elkészített .Net Core 7.0 alkalmazás, mely React 18-at használ Frontend oldalon.

---

2023 januárjában kezdtem el ezt a kurzust. Mivel C# nyelven tanultam asztali alkalimazásokat fejleszteni, így ebben a környezetben már elég konfortosan érzem magamat. Viszont úgy alakult, hogy a webprogramozást kicsivel jobban közelebb éreztem magamhoz, mint az asztali alkalimazások fejlesztését, így inkább PHP vonalon kezdtem el fejlődni. Azonban az utóbbi időben már kezdett hiányozni a C# nyelv, így elhatároztam, hogy elkezdem megtanulni a ASP.Net Core környezetben való fejlesztést is.

Eleinte a dokumentáció alapján próbáltam meg elsajátítani az alapokat és megismerni a .Net Core "lelkületét", viszont sajnos vagy nem sajnos nálam a "csinálom és közben tanulom" módszer vált csak be új ismeretek elsajátításánál. Ezért is kezdtem el ezt a kurzust, ahol nem csak a C# alapú Backend-et tudom megtanulni, hanem Frontend oldalon a ReactJS keretrendszert is. (Még úgy is, hogy nem tartom magamat valami nagy Frontend-esnek)

Ebben a repository-ban pedig nyomon lehet követni ahogy a kurzusban haladok, ugyan is minden commit egy-egy szekció végét jelzi, a projekt [Wiki oldalán](https://gitlab.com/molmar/reactivities/-/wikis/home) pedig összefoglalom, hogy egy-egy szekcióban pontosan mik történtek.